package com.htc.springboot.to;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CustomerTO {

	@NotEmpty(message = "Customer code cannot be empty")
	private String customerCode;
	@NotEmpty(message = "Customer name cannot be empty")
	private String customerName;
	@NotEmpty(message = "Address is mandatory")
	@Size(min = 10, max = 50, message = "Address must be between 10 to 50 characters")
	private String address;
	@NotEmpty(message = "Phone no cannot be empty")
	@Size(min = 10, max = 10, message = "Invalid Phoneno")
	private String contactNo;

	public CustomerTO() {
	}

	public CustomerTO(@NotEmpty(message = "Customer code cannot be empty") String customerCode,
			@NotEmpty(message = "Customer name cannot be empty") String customerName,
			@NotEmpty(message = "Address is mandatory") @Size(min = 10, max = 50, message = "Address must be between 10 to 50 characters") String address,
			@NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") @NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") @NotEmpty(message = "Phone no cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") String contactNo) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactNo = contactNo;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	@Override
	public String toString() {
		return "CustomerTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address=" + address
				+ ", contactNo=" + contactNo + "]";
	}

}
